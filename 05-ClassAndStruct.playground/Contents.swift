// MARK: Задание 1:
// Создать класс родитель и 2 класса наследника.
// Класс-родитель:
class Animal {
    var name: String
    
    init(name: String) {
        self.name = name
    }
    
    func makeSound() {
        print("Животное \(name) издает звук")
    }
}

// Класс-наследник 1:
class Cat: Animal {
    override func makeSound() {
        print("Кошка \(name) мурлычет")
    }
}

// Класс-наследник 2:
class Dog: Animal {
    override func makeSound() {
        print("Собака \(name) лает")
    }
}

let animal = Animal(name: "Животное")
let cat = Cat(name: "Барсик")
let dog = Dog(name: "Бобик")

animal.makeSound() // Выводит "Животное издает звук"
cat.makeSound()    // Выводит "Кошка Барсик мурлычет"
dog.makeSound()    // Выводит "Собака Бобик лает"



// MARK: Задание 2:
// Создать класс *House* в нем несколько свойств - *width*, *height* и несколько методов: *create*(выводит площадь),*destroy*(отображает что дом уничтожен).
class House {
    var width: Double
    var height: Double
    
    init(width: Double, height: Double) {
        self.width = width
        self.height = height
    }
    
    func create() {
        let area = width * height
        print("Дом создан, его площадь: \(area) квадратных метров.")
    }
    
    func destroy() {
        print("Дом уничтожен")
    }
}

let myHouse = House(width: 10, height: 5)
myHouse.create()  // Выводит "Дом создан, его площадь: 50.0 квадратных метров"
myHouse.destroy() // Выводит "Дом уничтожен"


// MARK: Задание 3:
// Создайте класс с методами, которые сортируют массив учеников по разным параметрам.
class Student {
    var name: String
    var age: Int
    var grade: Int
    
    init(name: String, age: Int, grade: Int) {
        self.name = name
        self.age = age
        self.grade = grade
    }
}

class Students {
    var students: [Student]
    
    init(students: [Student]) {
        self.students = students
    }
    
    func sortByAge() {
        students.sort { $0.age < $1.age }
    }
    
    func sortByName() {
        students.sort { $0.name < $1.name }
    }
    
    func sortByGrade() {
        students.sort { $0.grade > $1.grade }
    }
}

// Пример использования класса
let student1 = Student(name: "Иванов", age: 18, grade: 5)
let student2 = Student(name: "Петров", age: 17, grade: 4)
let student3 = Student(name: "Сидоров", age: 19, grade: 3)

let students = [student1, student2, student3]

let myClass = Students(students: students)

myClass.sortByAge()
print(myClass.students.map { $0.name }) // Выводит ["Петров", "Иванов", "Сидоров"]

myClass.sortByName()
print(myClass.students.map { $0.name }) // Выводит ["Иванов", "Петров", "Сидоров"]

myClass.sortByGrade()
print(myClass.students.map { $0.name }) // Выводит ["Иванов", "Петров", "Сидоров"]


// MARK: Задание 4:
// Написать свою структуру и класс, и пояснить в комментариях - чем отличаются структуры от классов.

// Пример структуры
struct Rectangle {
    var width: Double
    var height: Double
    
    func area() -> Double {
        return width * height
    }
}

// Пример класса
class Car {
    var make: String
    var model: String
    var year: Int
    
    init(make: String, model: String, year: Int) {
        self.make = make
        self.model = model
        self.year = year
    }
    
    func startEngine() {
        print("Двигатель запущен")
    }
}

/*
 Отличия между структурами и классами:
 
 1. Структуры - это значения, классы - это ссылки. При передаче структуры она копируется,
    а при передаче класса передается ссылка на объект.
 
 2. Структуры не могут наследоваться, классы могут наследоваться.
 
 3. В структурах свойства не могут быть изменены в методах, которые не являются mutating,
    в классах свойства могут быть изменены в любых методах.
 
 4. Структуры обычно используются для создания небольших объектов со значениями, а классы
    используются для создания более сложных объектов с поведением.
 */

// Пример использования структуры
let myRectangle = Rectangle(width: 10, height: 5)
print(myRectangle.area()) // Выводит 50.0

// Пример использования класса
let myCar = Car(make: "Toyota", model: "Corolla", year: 2020)
myCar.startEngine() // Выводит "Двигатель запущен"


// MARK: Задание 5:
// Напишите простую программу, которая отбирает комбинации в покере из рандомно выбранных 5 карт.
// Сохраняйте комбинации в массив.
// Если выпала определённая комбинация - выводим соответствующую запись в консоль.
// Результат в консоли примерно такой: 'У вас бубновый стрит флеш'.

// Определим возможные масти и значения карт
let suits = ["♠️", "♣️", "♥️", "♦️"]
let values = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"]

// Функция, которая генерирует и возвращает случайную карту
func randomCard() -> String {
    let randomSuit = suits.randomElement()!
    let randomValue = values.randomElement()!
    return randomValue + randomSuit
}

// Сгенерируем 5 случайных карт
var hand: [String] = []
for _ in 1...5 {
    hand.append(randomCard())
}

// Отсортируем карты по значению (2-10, J-Q-K-A)
hand.sort {
    if let index1 = values.firstIndex(of: String($0.prefix(1))), let index2 = values.firstIndex(of: String($1.prefix(1))) {
        return index1 < index2
    }
    return false
}

// Проверим наличие комбинаций
let isFlush = Set(hand.map { $0.suffix(1) }).count == 1 // флеш
let isStraight = values.firstIndex(of: String(hand[4].prefix(1)))! - values.firstIndex(of: String(hand[0].prefix(1)))! == 4 // стрит
let isStraightFlush = isFlush && isStraight // стрит-флеш
let isFourOfAKind = Set(hand.map { $0.prefix(1) }).count == 2 // каре
let isFullHouse = Set(hand.map { $0.prefix(1) }).count == 2 && Set(hand.map { $0.suffix(1) }).count == 1 // фулл-хаус
let isThreeOfAKind = Set(hand.map { $0.prefix(1) }).count == 3 // тройка
let isTwoPairs = Set(hand.map { $0.prefix(1) }).count == 3 && Set(hand.map { $0.suffix(1) }).count == 2 // две пары
let isPair = Set(hand.map { $0.prefix(1) }).count == 4 // пара

// Выведем результат
if isStraightFlush {
    print("У вас стрит-флеш")
} else if isFourOfAKind {
    print("У вас каре")
} else if isFullHouse {
    print("У вас фулл-хаус")
} else if isFlush {
    print("У вас флеш")
} else if isStraight {
    print("У вас стрит")
} else if isThreeOfAKind {
    print("У вас тройка")
} else if isTwoPairs {
    print("У вас две пары")
} else if isPair {
    print("У вас пара")
} else {
    print("У вас высокая карта")
}

// Выведем сгенерированные карты
print("Ваши карты: \(hand.joined(separator: ", "))")
